export class Shuffle {
    public static readonly type = '[App] Shuffle';
}

export class SetSelectedIndex {
    public static readonly type = '[App] SetSelectedIndex';
    constructor(public selectedIndex: number) { }
}

export class ResetIndexSelected {
    public static readonly type = '[App] ResetIndexSelected';
}

export class FirstStep {
    public static readonly type = '[App] FirstStep';
    constructor(public selectedIndex: number) { }
}

export class SecondStep {
    public static readonly type = '[App] SecondStep';
    constructor(public selectedIndex: number) { }
}

export class CheckingMatches {
    public static readonly type = '[App] CheckingMatches';
}

export class Reset {
    public static readonly type = '[App] Reset';
}

export class Play {
    public static readonly type = '[App] Play';
}

export class ResetMatchIndex {
    public static readonly type = '[App] ResetMatchIndex';
}

export class CheckWinner {
    public static readonly type = '[App] CheckWinner';
}

export class YouLost {
    public static readonly type = '[App] YouLost';
}
