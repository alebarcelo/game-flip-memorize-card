import {Action, Selector, State, StateContext} from '@ngxs/store';
import {
    CheckingMatches, CheckWinner,
    FirstStep,
    Play, Reset,
    ResetIndexSelected,
    ResetMatchIndex,
    SecondStep,
    SetSelectedIndex,
    Shuffle, YouLost
} from './app.actions';
import * as _ from 'lodash';

export interface AppStateModel {
    cards: number[];
    selectedIndex: number[];
    step: 'first' | 'second' | 'waiting';
    state: 'ready' | 'playing' | 'looser' | 'winner';
    failIndex: number[];
    matchIndex: number[];
}

const initialState: AppStateModel = {
    cards: [],
    selectedIndex: [],
    step: 'first',
    state: 'ready',
    failIndex: [],
    matchIndex: []
};

@State<AppStateModel>({
    name: 'app',
    defaults: initialState
})
export class AppState {
    constructor() {
    }

    @Selector()
    public static cards(state: AppStateModel) {
        return state.cards;
    }

    @Selector()
    public static isReady(state: AppStateModel) {
        return state.state === 'ready';
    }

    @Selector()
    public static isPlaying(state: AppStateModel) {
        return state.state === 'playing';
    }

    @Selector()
    public static isWinner(state: AppStateModel) {
        return state.state === 'winner';
    }

    @Selector()
    public static isLooser(state: AppStateModel) {
        return state.state === 'looser';
    }

    @Selector()
    public static isWaiting(state: AppStateModel) {
        return state.step === 'waiting';
    }

    @Selector()
    public static selectedIndex(state: AppStateModel) {
        return state.selectedIndex;
    }

    @Selector()
    public static failIndex(state: AppStateModel) {
        return state.failIndex;
    }

    @Selector()
    public static matchIndex(state: AppStateModel) {
        return state.matchIndex;
    }

    @Action(Reset)
    public reset({setState}: StateContext<AppStateModel>) {
        setState(initialState);
    }

    @Action(Play)
    public play({getState, patchState}: StateContext<AppStateModel>) {
        patchState({
            state: 'playing'
        });
    }

    @Action(Shuffle)
    public shuffle({getState, patchState}: StateContext<AppStateModel>) {
        patchState({cards: _.shuffle(_.concat(_.range(1, 10), _.range(1, 10)))});
    }

    @Action(FirstStep)
    public firstStep({getState, patchState}: StateContext<AppStateModel>, {selectedIndex}: FirstStep) {
        const state = getState();

        patchState({
            selectedIndex: [...state.selectedIndex, selectedIndex],
            step: 'second'
        });
    }

    @Action(SecondStep)
    public secondStep({getState, patchState}: StateContext<AppStateModel>, {selectedIndex}: SecondStep) {
        const state = getState();

        patchState({
            selectedIndex: [...state.selectedIndex, selectedIndex],
            step: 'waiting'
        });
    }

    @Action(SetSelectedIndex)
    public setSelectedIndex({getState, patchState, dispatch}: StateContext<AppStateModel>, {selectedIndex}: SetSelectedIndex) {
        const state = getState();

        if ( state.step === 'first' ) {
            dispatch(new FirstStep(selectedIndex));
        } else if ( state.step === 'second' ) {
            dispatch([new SecondStep(selectedIndex), new CheckingMatches()]);
        }
    }

    @Action(CheckingMatches)
    public checkingMatches({getState, patchState, dispatch}: StateContext<AppStateModel>) {
        const state = getState();
        const forCheck = _.takeRight(state.selectedIndex, 2);

        if ( state.cards[forCheck[0]] === state.cards[forCheck[1]] ) {
            setTimeout(() => {
                patchState({
                    step: 'first',
                    matchIndex: forCheck
                });
                dispatch(new CheckWinner());
            }, 500);
            setTimeout(() => {
                dispatch(new ResetMatchIndex());
            }, 1000);
        } else {
            setTimeout(() => {
                patchState({
                    failIndex: forCheck
                });
            }, 500);
            setTimeout(() => {
                dispatch(new ResetIndexSelected());
            }, 1000);
        }
    }

    @Action(ResetIndexSelected)
    public resetIndexSelected({getState, patchState}: StateContext<AppStateModel>) {
        const state = getState();
        const selectedIndex = _.clone(state.selectedIndex);

        selectedIndex.splice(-2, 2);
        patchState({
            selectedIndex,
            step: 'first',
            failIndex: []
        });
    }

    @Action(ResetMatchIndex)
    public resetMismatchIndex({getState, patchState}: StateContext<AppStateModel>) {
        patchState({
            matchIndex: []
        });
    }

    @Action(CheckWinner)
    public checkWinner({getState, patchState}: StateContext<AppStateModel>) {
        const state = getState();

        if (state.selectedIndex.length === state.cards.length) {
            patchState({
                state: 'winner'
            });
        }
    }

    @Action(YouLost)
    public youLost({patchState}: StateContext<AppStateModel>) {
        patchState({
            state: 'looser'
        });
    }
}
