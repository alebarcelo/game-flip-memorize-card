import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import { NgxsModule } from '@ngxs/store';
import { CardComponent } from './components/card/card.component';
import { BoardComponent } from './components/board/board.component';
import {AppState} from "./store/app.state";
import {environment} from "../environments/environment";
import {NgxsReduxDevtoolsPluginModule} from "@ngxs/devtools-plugin";
import { MarkerComponent } from './components/marker/marker.component';
import { CountdownModule } from 'ngx-countdown';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {NgbModalModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    declarations: [
        AppComponent,
        CardComponent,
        BoardComponent,
        MarkerComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        CountdownModule,
        FontAwesomeModule,
        NgbModalModule,
        NgxsModule.forRoot([AppState], {developmentMode: !environment.production}),
        NgxsReduxDevtoolsPluginModule.forRoot()
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
