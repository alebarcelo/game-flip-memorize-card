import {Component, Input, OnInit} from '@angular/core';
import {Select, Store} from "@ngxs/store";
import {SetSelectedIndex} from "../../store/app.actions";
import {AppState} from "../../store/app.state";
import {Observable} from "rxjs";
import {delay, find, map} from 'rxjs/operators';

@Component({
    selector: 'app-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
    @Input() data: number;
    @Input() index: number;

    @Select(AppState.selectedIndex)
    selectedIndex$: Observable<number[]>;

    @Select(AppState.failIndex)
    failIndex$: Observable<number[]>;

    @Select(AppState.matchIndex)
    matchIndex$: Observable<number[]>;

    @Select(AppState.isWaiting)
    isWaiting$: Observable<boolean>;

    @Select(AppState.isReady)
    isReady$: Observable<boolean>;

    constructor(private store: Store) {
    }

    ngOnInit() {
    }

    get isSelected$(): Observable<boolean> {
        return this.selectedIndex$.pipe(
            map((selected: number[]) => {
                return selected.indexOf(this.index) > -1;
            })
        );
    }

    get isFailed$(): Observable<boolean> {
        return this.failIndex$.pipe(
            map((failIndex: number[]) => {
                return failIndex.indexOf(this.index) > -1;
            })
        );
    }

    get isMatch$(): Observable<boolean> {
        return this.matchIndex$.pipe(
            map((matchIndex: number[]) => {
                return matchIndex.indexOf(this.index) > -1;
            })
        );
    }

    setSelected() {
        const selectedIndex = this.store.selectSnapshot(AppState.selectedIndex);
        const isWaiting = this.store.selectSnapshot(AppState.isWaiting);
        const isPlaying = this.store.selectSnapshot(AppState.isPlaying);

        if ( isPlaying && !isWaiting && selectedIndex.indexOf(this.index) === -1 ) {
            this.store.dispatch(new SetSelectedIndex(this.index));
        }
    }
}
