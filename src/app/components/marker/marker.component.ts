import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Config, CountdownComponent} from 'ngx-countdown';
import {faClock} from '@fortawesome/free-regular-svg-icons';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {Select, Store} from '@ngxs/store';
import {Play, Reset, Shuffle, YouLost} from '../../store/app.actions';
import {AppState} from '../../store/app.state';
import {Observable} from 'rxjs';
import {filter} from 'rxjs/operators';
import {NgbModal, NgbModalConfig, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-marker',
    templateUrl: './marker.component.html',
    styleUrls: ['./marker.component.scss'],
    providers: [NgbModalConfig, NgbModal]
})
export class MarkerComponent implements OnInit {
    @ViewChild('counter', {static: true}) counter: CountdownComponent;
    @ViewChild('modalWinnerRef', {static: true}) modalWinnerRef: ElementRef;
    @ViewChild('modalLooserRef', {static: true}) modalLooserRef: ElementRef;
    @Select(AppState.isPlaying)
    isPlaying$: Observable<boolean>;
    @Select(AppState.isReady)
    isReady$: Observable<boolean>;
    @Select(AppState.isWinner)
    isWinner$: Observable<boolean>;
    @Select(AppState.isLooser)
    isLooser$: Observable<boolean>;

    modalRef: NgbModalRef;

    markerConfig: Config = {
        leftTime: 120,
        demand: true
    };

    constructor(
        private library: FaIconLibrary,
        private store: Store,
        private modalService: NgbModal,
        config: NgbModalConfig) {
        config.backdrop = 'static';
        config.keyboard = false;
        library.addIcons(faClock);
    }

    ngOnInit() {

        this.isWinner$.pipe(filter(isWinner => !!isWinner))
            .subscribe(isWinner => {
                this.counter.stop();
                this.modalRef = this.modalService.open(this.modalWinnerRef, { size: 'lg', centered: true });
                this.modalRef.result.then(() => {
                    this.ready();
                });
            });

        this.isLooser$.pipe(filter(isLooser => !!isLooser))
            .subscribe(isLooser => {
                this.counter.stop();
                this.modalRef = this.modalService.open(this.modalLooserRef, { size: 'lg', centered: true });
                this.modalRef.result.then(() => {
                    this.ready();
                });
            });
    }

    ready() {
        this.store.dispatch([new Reset(), new Shuffle()]);
    }

    play() {
        this.counter.restart();
        this.store.dispatch(new Play());
        this.counter.begin();
    }

    countDownFinished($event) {
        this.store.dispatch(new YouLost());
    }
}
