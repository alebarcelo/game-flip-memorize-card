import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import {Select} from "@ngxs/store";
import {AppState} from "../../store/app.state";
import {Observable} from "rxjs";

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  @Select(AppState.cards)
  cards$: Observable<number[]>;

  constructor() { }

  ngOnInit() {
  }

}
